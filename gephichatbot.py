import csv
import json
import sys
from pprint import pprint
import os
import re

intentdir = 'Intents'
entitydir = 'Entities'
class GephiChatbot:
    def __init__(self, csv_filename, json_filename):
        if csv_filename!= None :
            self.csv_file = csv_filename

        if json_filename!= None :
            self.json_file = json_filename
    
    def csv_to_json(self, verticies_csv_table, edges_csv_table, output_json_file):
        with open(verticies_csv_table, 'r') as vt:
            with open(edges_csv_table, 'r') as et:
                vt_fieldnames = next(csv.reader(vt))
                et_fieldnames = next(csv.reader(et))
                 
                reader = csv.DictReader(vt, vt_fieldnames)
                vt_info = []
                for row in reader:
                    vt_info.append(row)
                json.dump(vt_info, open('KnotenTabelle.json', 'w'))

                reader = None

                reader = csv.DictReader(et, et_fieldnames)
                et_info = []
                for row in reader:
                    et_info.append(row)
                json.dump(et_info, open('KantenTabelle.json', 'w'))

                with open('ibm_config.json', 'r') as c:
                    output_object = json.loads(c.read())
                    json.dump(output_object, open(output_json_file, 'w'))

                new_graph = {
                    "verticies": getVertInformation(vt_info),
                    "edges": getEdgeInformtation(et_info)
                }
                dialog_nodes = constructDialogNodes(new_graph, 0)

                intents = getIntents(new_graph)
                entities = getEntities(new_graph)
                    
                output_object = json.loads(open(output_json_file, 'r').read())
                output_object["dialog_nodes"] = dialog_nodes
                output_object["intents"] = intents
                output_object["entities"] = entities
                json.dump(output_object, open(output_json_file, 'w'))

    def json_to_csv(self, chatbot_json_file, output_vert_csv_file, output_edge_csv_file):
        chatbot_object = json.loads(open(chatbot_json_file,  encoding="utf8").read())
        csv_vert_data= []
        csv_edge_data = []

        vert_header = ["Id", "Label", "tyoe", "robotsays", "response_type", "selection_policy"]
        edge_header = ["source", "target", "intents", "entities", "other"]

        for vert in chatbot_object["dialog_nodes"]:
            vert_data = []
            vert_data.append(vert["dialog_node"])
            vert_data.append(vert["title"])
            vert_data.append("type")
            if "generic" in vert["output"]:
                if len(vert["output"]["generic"]) > 1:
                    entries = []
                    
                    for entry in range(len(vert["output"]["generic"])):
                        entries.append(entry["text"]["values"])

            csv_vert_data.append(vert_data)
            print(csv_vert_data)

            if "parent" in vert:
                edge_data = []
                edge_data.append(vert["parent"])
                edge_data.append(vert["dialog_node"])
                if "conditions" in vert and vert["conditions"] != "":
                    if isinstance(vert["conditions"], str):
                        if vert["conditions"][0] == '#':
                            edge_data.append(vert["conditions"])
                            edge_data.append("None")
                        elif vert["conditions"][0] == '@':
                            edge_data.append("None")
                            edge_data.append(vert["conditions"])
                    else:
                        intents = []
                        entities = []
                        for condition in vert["conditions"]:
                            if condition[0] == "#":
                                intents.append(condition)
                            elif condition[0] == "@":
                                entities.append(condition)
                        edge_data.append(intents)
                        edge_data.append(entities)
                else:
                    edge_data.append("None")
                    edge_data.append("None")
                if "next_step" in vert:
                    edge_data.append(vert["next_step"]["behavior"])
                else: 
                    edge_data.append("None")
                csv_edge_data.append(edge_data)
            print(csv_edge_data)
        
        with open(output_vert_csv_file, 'w') as fp:
            writer = csv.writer(fp, delimiter=',')
            writer.writerow(vert_header)
            for row in csv_vert_data:
                writer.writerow(row)
        
        writer = None

        with open(output_edge_csv_file, 'w') as fp:
            writer = csv.writer(fp, delimiter=',')
            writer.writerow(edge_header)
            for row in csv_edge_data:
                writer.writerow(row)
        





                

def getVertInformation(vt_info):
    verticies = []
    for v in range(len(vt_info)):
        verticy = {
            "dialog_nodes": vt_info[v]["dialog_nodes"],
            "values": vt_info[v]["values"],
            "Id": vt_info[v]["Id"]
        }
        verticies.append(verticy)
    return verticies

def getEdgeInformtation(et_info):
    edges = []
    for e in range(len(et_info)):
        edge = {
            "source": et_info[e]["source"],
            "target": et_info[e]["target"],
            "Id": et_info[e]["Id"],
            "intents": et_info[e]["intents"].split(','),
            "entities": et_info[e]["entities"].split(','),
            "other": et_info[e]["other"]            
        }
        edges.append(edge)
    return edges

def constructDialogNodes(graph, rootId, dialog_nodes=[], conditions="welcome", other=None, parent=None, generic_number=0, previous_sibling = None):
    next_step = None
    if other== 'always':
        next_step={
            "behavior":"skip_user_input"
        }

    generic_Id = graph["verticies"][rootId]["dialog_nodes"] + "_" + str(generic_number)

    dialog_node={
        "type": "standard",
        "title": graph["verticies"][rootId]["dialog_nodes"],
        "output":{
            "text":{
                "values": graph["verticies"][rootId]["values"],
                "selection_policy": "sequential"
            }
        },
        "parent": parent,
        "metadata": {},
        "next_step": next_step,
        "conditions": conditions,
        "dialog_node": generic_Id,
        "previous_sibling": previous_sibling
    }

    clean(dialog_node, ["parent", "next_step", "conditions", "previous_sibling"])
    dialog_nodes.append(dialog_node)

    previous_sibling = None
    for e in range(len(graph["edges"])):
        generic_number+= 1
        if int(graph["edges"][e]["source"]) == rootId:
            constructDialogNodes(graph, int(graph["edges"][e]["target"]), dialog_nodes, ' or '.join(list(filter(lambda entry : entry != '', graph["edges"][e]["intents"]+graph["edges"][e]["entities"]))) if graph["edges"][e]["intents"]+graph["edges"][e]["entities"]!= "" else None, graph["edges"][e]["other"] , generic_Id, generic_number, previous_sibling)
            previous_sibling = "generic_number_"+str(generic_number)
    return dialog_nodes

def getIntents(graph):
    intents =[]
    for e in range(len(graph["edges"])):
        if graph["edges"][e]["intents"] != None and graph["edges"][e]["intents"] != '':
            for i in range(len(graph["edges"][e]["intents"])):
                intent = graph["edges"][e]["intents"][i]
                if intent != None and intent != '':
                    intentfilename = intent[1:]+".txt"
                    intentfile = open(intentdir+os.sep+intentfilename, 'r')
                    examples = intentfile.read().split('\n')
                    exampleObjects = []
                    for ex in range(len(examples)):
                        exampleObjects.append({
                            "text": examples[ex]
                        })
                    intents.append({
                        "intent":intent[1:],
                        "examples": exampleObjects,
                        "description":''
                    })
                    intentfile.close
    return intents

def getEntities(graph):
    entities =[]
    for e in range(len(graph["edges"])):
        if graph["edges"][e]["entities"] != None and graph["edges"][e]["entities"] != '':
            for en in range(len(graph["edges"][e]["entities"])):
                entity = graph["edges"][e]["entities"][en]
                if entity != None and entity != '':
                    entityfilename = entity[1:]+".txt"
                    entityfile = open(entitydir+os.sep+entityfilename, 'r')
                    synonyms = entityfile.read().split('\n')
                    values =[]
                    values.append({
                        "type": "syonyms", 
                        "value": entity[1:],
                        "synonyms":synonyms
                    })
                    entities.append({
                        "entity": entity[1:],
                        "values": values,
                        "fuzzy_match": True
                    })
    return entities

def clean(obj, attributesToCheck):
    for attribute in attributesToCheck: 
        if obj[attribute] == None:
            del obj[attribute]
            


def main():
    print(sys.argv)
    gephichatbot = GephiChatbot(None, None)
    #gephichatbot.csv_to_json('KnotenTabelle.csv', 'KantenTabelle.csv', 'ProgrammOutput.json')
    gephichatbot.json_to_csv('skill_corobi.json', 'Knoten_ProgrammOutput.csv', 'Kanten_ProgrammOutput.csv')


if __name__ == "__main__":
    main()